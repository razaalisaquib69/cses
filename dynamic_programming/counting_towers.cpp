#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_bigdate>
#define vll vector<ll>
#define pll pair<ll, ll>
#define vpll vector<pll>
#define ff first
#define ss second
#define ll long long
#define inf LLONG_MAX
#define mod 1000000007
using namespace std;
 
vector<vector<ll>> dp(1e6+7,vector<ll>(2));

void solve(int tc){
    ll N;
    cin>>N;
    dp[N][0] = 1;
    dp[N][1] = 1;
    // dp[i][j] = no. of ways to fill the tower from the ith row to the n-1th row givne the state of the i-1th row 
    // j -> 0 == horizontal filling
    // j -> 1 == vertical filling
    for(int i = N-1; i >= 0; i--){
        dp[i][0] = (dp[i+1][0]*2 + dp[i+1][1])%mod;
        dp[i][1] = (dp[i+1][1]*4 + dp[i+1][0])%mod;
    }
    cout<<(dp[1][0]+dp[1][1])%mod<<endl;
}

int main()
{

#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    // t = 1;
    for(int i = 1; i <= t; i++){
	    solve(i);
    }
    return 0;
}