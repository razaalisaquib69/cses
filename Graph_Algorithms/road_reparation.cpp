#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_bigdate>
#define vll vector<ll>
#define pll pair<ll, ll>
#define vpll vector<pll>
#define rep(a, b, c) for (ll a = b; a < c; a++)
#define rrep(a, b, c) for (ll a = b; a >= c; a--)
#define ff first
#define ss second
#define ll long long
#define inf LLONG_MAX
#define mod 1000000007
using namespace std;
 
class DisjointSet {
    vector<ll> rank, parent;
public:
    DisjointSet(ll n) {
        rank.resize(n + 1, 0);
        parent.resize(n + 1);
        for (int i = 0; i <= n; i++) {
            parent[i] = i;
        }
    }
 
    ll findUPar(ll node) {
        if (node == parent[node])
            return node;
        return parent[node] = findUPar(parent[node]);
    }
 
    void unionByRank(ll u, ll v) {
        ll ulp_u = findUPar(u);
        ll ulp_v = findUPar(v);
        if (ulp_u == ulp_v) return;
        if (rank[ulp_u] < rank[ulp_v]) {
            parent[ulp_u] = ulp_v;
        }
        else if (rank[ulp_v] < rank[ulp_u]) {
            parent[ulp_v] = ulp_u;
        }
else {
            parent[ulp_v] = ulp_u;
            rank[ulp_u]++;
        }
    }
};
 
void solve() {
    ll n,m;
    cin>>n>>m;
    vector<pair<ll,pair<ll,ll>>> v;
    for(int i = 0; i < m; i++){
        ll a,b,w;
        cin>>a>>b>>w;
        v.push_back({w,{a,b}});
    }
    sort(v.begin(),v.end());
    DisjointSet dst(n);
    ll cost = 0;
    for(int i = 0; i < m; i++){
        ll node_a = v[i].ss.ff;
        ll node_b = v[i].ss.ss;
        ll weight = v[i].ff;
        ll parent_a = dst.findUPar(node_a);
        ll parent_b = dst.findUPar(node_b);
        if(parent_a != parent_b){
            dst.unionByRank(node_a,node_b);
            cost += weight;
        }
    }
    int commont_parent = dst.findUPar(1);
    for(int j = 2; j <= n; j++){
        if(dst.findUPar(j) != commont_parent){
            cout<<"IMPOSSIBLE";
            return;
        }
    }
    cout<<cost<<endl;
}
 
int main()
{
 
#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    // cin>>t;
    t = 1;
    while(t--){
	    solve();
    }
    return 0;
}
