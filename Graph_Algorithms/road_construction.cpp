#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_bigdate>
#define vll vector<ll>
#define pll pair<ll, ll>
#define vpll vector<pll>
#define rep(a, b, c) for (ll a = b; a < c; a++)
#define rrep(a, b, c) for (ll a = b; a >= c; a--)
#define ff first
#define ss second
#define ll long long
#define inf LLONG_MAX
#define mod 1000000007
using namespace std;

class DisjointSet {
    vector<int> rank, parent, size;
public:
    DisjointSet(int n) {
        parent.resize(n + 1);
        size.resize(n + 1);
        for (int i = 0; i <= n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
    }

    int findUPar(int node) {
        if (node == parent[node])
            return node;
        return parent[node] = findUPar(parent[node]);
    }

    void unionBySize(int u, int v, ll& components, ll& maxi) {
        int ulp_u = findUPar(u);
        int ulp_v = findUPar(v);
        if (ulp_u == ulp_v) return;
        if (size[ulp_u] < size[ulp_v]) {
            parent[ulp_u] = ulp_v;
            size[ulp_v] += size[ulp_u];
            maxi = max(maxi,1ll*size[ulp_v]);
        }
        else {
            parent[ulp_v] = ulp_u;
            size[ulp_u] += size[ulp_v];
            maxi = max(maxi,1ll*size[ulp_u]);
        }
        components--;
    }
};


void solve() {
    ll n,m;
    cin>>n>>m;
    DisjointSet dst(n);
    ll components = n, maxi = 1;
    for(int i = 0; i < m; i++){
        int u,v;
        cin>>u>>v;
        dst.unionBySize(u,v,components, maxi);
        cout<<components<<" "<<maxi<<endl;
    }
}

int main()
{

#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    // cin>>t;
    t = 1;
    while(t--){
	    solve();
    }
    return 0;
}