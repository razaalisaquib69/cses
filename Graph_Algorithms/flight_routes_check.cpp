#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_bigdate>
#define vll vector<ll>
#define pll pair<ll, ll>
#define vpll vector<pll>
#define rep(a, b, c) for (ll a = b; a < c; a++)
#define rrep(a, b, c) for (ll a = b; a >= c; a--)
#define ff first
#define ss second
#define ll long long
#define inf LLONG_MAX
#define mod 1000000007
using namespace std;

void dfs(vll adj[], vector<bool>& vis, stack<int>& st,int node){
    vis[node] = 1;
    for(auto &it: adj[node]){
        if(!vis[it]){
            dfs(adj,vis,st,it);
        }
    }
    st.push(node);
    return;
}

void dfs_2(vll adj_2[], vector<bool>& vis_2,int node){
    vis_2[node] = true;
    for(auto &it : adj_2[node]){
        if(!vis_2[it]){
            dfs_2(adj_2,vis_2,it);
        }
    }
}


void solve() {
    ll n,m;
    cin>>n>>m;
    vll adj[n];
    vll adj_2[n];
    for(int i = 0; i < m; i++){
        ll u,v;
        cin>>u>>v;
        u--;
        v--;
        adj[u].push_back(v);
        adj_2[v].push_back(u);
    }
    vector<bool> vis(n,false);
    stack<int> st;
    for(int i = 0; i < n; i++){
        if(!vis[i]){
            dfs(adj,vis,st,i);
        }
    }
    for(int i = 0; i < n; i++){
        vis[i] = false;
    }
    while(!st.empty()){
        int i = st.top();
        st.pop();
        dfs_2(adj_2,vis,i);
        break;
    }
    while(!st.empty()){
        int i = st.top();
        if(vis[i]){
            st.pop();
        }else{
            break;
        }
    }
    if(!st.empty()){
        cout<<"NO\n";
        int u,v;
        for(int i = 0; i < n; i++){
            if(vis[i] == false){
                u = i+1;
            }else{
                v = i+1;
            }
        }
        cout<<u<<" "<<v;
    }else{
        cout<<"YES\n";
    }
}

int main()
{

#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    // cin>>t;
    t = 1;
    while(t--){
	    solve();
    }
    return 0;
}