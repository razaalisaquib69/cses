#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
#define ordered_set tree<int, null_type, less<int>, rb_tree_tag, tree_order_statistics_node_bigdate>
#define vll vector<ll>
#define pll pair<ll, ll>
#define vpll vector<pll>
#define rep(a, b, c) for (ll a = b; a < c; a++)
#define rrep(a, b, c) for (ll a = b; a >= c; a--)
#define ff first
#define ss second
#define ll long long
#define inf LLONG_MAX
#define mod 1000000007
using namespace std;

void solve() {
    // taking inputs
    ll n;
    cin>>n;
    vll next_node(n+1);
    for(ll i = 1; i <= n; i++){
        cin>>next_node[i];
    }

    vll vis(n+1, false);
    vll res(n+1,-1);

    for(ll i = 1; i <= n; i++){
        if(vis[i] == 0){
            unordered_set<ll> st;
            vll temp_vec;
            ll curr_node = i;
            // dfs
            while(st.find(curr_node) == st.end()){
                st.insert(curr_node);
                temp_vec.push_back(curr_node);
                if(vis[curr_node]){
                    break;
                }
                curr_node = next_node[curr_node];
            }
            ll ind_repeated_node = find(temp_vec.begin(), temp_vec.end(), curr_node) - temp_vec.begin();
            ll m = temp_vec.size();
            // marking all the nodes which came after the repeated node as part of the cycle and the number of nodes they can finally 
            // reach is equal to the size of the cycle which is m - ind_repeated_node.
            for (ll i = ind_repeated_node; i < m; i++)
            {
                vis[temp_vec[i]] = 1;
                if (res[temp_vec[i]] == -1)
                    res[temp_vec[i]] = m - ind_repeated_node;
            }
            // these are all the nodes which point towards a node in the cycle for these nodes ans[x] = ans[nextnode[x]]+1
            for (ll i = ind_repeated_node - 1; i >= 0; i--)
            {
                vis[temp_vec[i]] = 1;
                if (res[temp_vec[i]] == -1)
                    res[temp_vec[i]] = res[temp_vec[i + 1]] + 1;
            }
        }
    }
    for (ll i = 1; i <= n; i++)
        cout << res[i] << " ";
}

int main()
{

#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    // cin>>t;
    t = 1;
    while(t--){
	    solve();
    }
    return 0;
}
