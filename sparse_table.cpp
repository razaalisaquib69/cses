const int N = 200007;
const int LOG = 18;

ll sparseTable[N][LOG];

void buildSparseTable(vll& v){
    for(int i = 0; i < v.size(); i++){
        sparseTable[i][0] = v[i];
    }
    for(int k = 1; k < LOG; k++){
        for(int i = 0; i + (1 << k)-1 < v.size(); i++){
            sparseTable[i][k] = min(sparseTable[i][k-1],sparseTable[i+(1<<(k-1))][k-1]);
        }
    }
}

ll query(ll l, ll r){
    ll len = r-l+1;
    ll k = 0;
    while((1 << (k+1)) <= len){
        k++;
    }
    return min(sparseTable[l][k],sparseTable[r-(1<<k)+1][k]);
}
